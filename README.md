# Dungeon generator

This repository is used for experimentation only.

A proper demo of the dungeon generator can be found here:

[https://gitlab.com/olaron/dungeon-generator-demo](https://gitlab.com/olaron/dungeon-generator-demo)

# Build

## Build clingo's static library

```bash
cmake -DCLINGO_BUILD_WEB=Off -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_FLAGS="-std=c++11 -Wall" -DCMAKE_CXX_FLAGS_RELEASE="-Os -DNDEBUG" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" ../..

make
```

## Build room.cpp in binary

```bash
clang++ -I include -L lib/bin -std=c++11 room.cpp -lclingo -lgringo -lclasp -lpotassco -lreify -o build/bin/room
```

## Build clingo's static library for wasm

```bash
emcmake cmake -DCLINGO_BUILD_WEB=On -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-std=c++11 -Wall" -DCMAKE_CXX_FLAGS_RELEASE="-Os -DNDEBUG" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" ../..

emmake make
```

## bc

```bach
cmake -DCLINGO_BUILD_WEB=Off -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_FLAGS="-std=c++11 -Wall -emit-llvm -c" -DCMAKE_CXX_FLAGS_RELEASE="-O3" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" ../..

make

...

em++ -I include -L lib/bc -s WASM=1 -std=c++11 room.cpp -lclingo -lgringo -lclasp -lpotassco -lreify -o build/bc/room.html
```

## em

```bash
emcmake cmake -DCLINGO_BUILD_WEB=Off -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-std=c++11 -Wall" -DCMAKE_CXX_FLAGS_RELEASE="-O3" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" ../..

emmake make
```

build libgringo.bc using .o files
```
em++ -Wall -o libgringo.bc @CMakeFiles/libgringo.dir/objects1.rsp
```

# win

```bach
cmake -DCLINGO_BUILD_WEB=Off -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -G"Visual Studio 14 2015" -DCMAKE_CXX_FLAGS="/Wall /EHsc" -DCMAKE_CXX_FLAGS_RELEASE="/O2" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" ../..

cmake --build .
```

```bach
cmake -DCLINGO_BUILD_WEB=Off -DCLINGO_BUILD_STATIC=On -DCLINGO_BUILD_WITH_PYTHON=Off -DCLINGO_BUILD_WITH_LUA=Off -DCLINGO_BUILD_SHARED=Off -DCLASP_BUILD_WITH_THREADS=Off -DCMAKE_VERBOSE_MAKEFILE=On -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS="-std=c++11 -Wall" -DCMAKE_CXX_FLAGS_RELEASE="-O3" -DCMAKE_EXE_LINKER_FLAGS="" -DCMAKE_EXE_LINKER_FLAGS_RELEASE="" -G"MinGW Makefiles" ../..

cmake --build .
```

# fmod for mingw

```bash
gendef - fmod.dll | sed "s/^_//" > fmod.def
dlltool -U -d fmod.def -l libfmoddll.a

gendef - fmodstudio.dll | sed "s/^_//" > fmodstudio.def
dlltool -U -d fmodstudio.def -l libfmodstudiodll.a
```