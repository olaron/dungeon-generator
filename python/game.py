import sys, pygame
from room import Matrix, Assumptions, RoomGenerator


class SpriteSheet(object):
    def __init__(self, filename):
        try:
            self.sheet = pygame.image.load(filename).convert()
        except pygame.error:
            print('Unable to load spritesheet image:', filename)
            raise SystemExit

    # Load a specific image from a specific rectangle
    def image_at(self, rectangle, colorkey=None) -> pygame.Surface:
        """Loads image from x,y,x+offset,y+offset"""
        rect = pygame.Rect(rectangle)
        image = pygame.Surface(rect.size).convert()
        image.blit(self.sheet, (0, 0), rect)
        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at((0, 0))
            image.set_colorkey(colorkey, pygame.RLEACCEL)
        return image

    # Load a whole bunch of images and return them as a list
    def images_at(self, rects, colorkey=None):
        """Loads multiple images, supply a list of coordinates"""
        return [self.image_at(rect, colorkey) for rect in rects]

    # Load a whole strip of images
    def load_strip(self, rect, image_count, colorkey=None):
        """Loads a strip of images and returns them as a list"""
        tups = [(rect[0] + rect[2] * x, rect[1], rect[2], rect[3])
                for x in range(image_count)]
        return self.images_at(tups, colorkey)


pygame.display.init()

black = 0, 0, 0
framerate = 60
loading_framerate = 25
spritesize = 16
spritescale = 4 * spritesize
size = width, height = 9 * spritescale, 9 * spritescale

screen: pygame.Surface = pygame.display.set_mode(size)

tileset = SpriteSheet("tileset.png")
tiles = {}
tiles["o"] = pygame.transform.scale(tileset.image_at((0, 0, spritesize, spritesize)), (spritescale, spritescale))
tiles["x"] = pygame.transform.scale(tileset.image_at((spritesize, 0, spritesize, spritesize)),
                                    (spritescale, spritescale))
tiles["b"] = pygame.transform.scale(tileset.image_at((spritesize * 2, 0, spritesize, spritesize)),
                                    (spritescale, spritescale))
tiles["d"] = pygame.transform.scale(tileset.image_at((spritesize * 3, 0, spritesize, spritesize)),
                                    (spritescale, spritescale))
tiles["s"] = pygame.transform.scale(tileset.image_at((spritesize * 4, 0, spritesize, spritesize)),
                                    (spritescale, spritescale))
tiles["l"] = pygame.transform.scale(tileset.image_at((spritesize * 5, 0, spritesize, spritesize)),
                                    (spritescale, spritescale))

clock = pygame.time.Clock()

generator = RoomGenerator("../resources/room.ans")
generator.ground()

a = Assumptions()
a.traversable(start_dir="w", end_dir="e", truth=True)
a.traversable(start_dir="w", end_dir="n", truth=True)
a.traversable(start_dir="s", end_dir="e", truth=True)
a.traversable(start_dir="s", end_dir="n", truth=False)
a.traversable(start_dir="s", end_dir="n", start_keys=[1], end_keys=[0], end_door="b", truth=True)
# a.open(dir="n",truth=False)

# a.traversable(start_dir="e",end_dir="w",truth=True)
# a.traversable(start_dir="s",end_dir="w",truth=False)

# a.no_switch()
# a.no_lock()
a.no_keys()

solver = None
ready = False
room = None


def new_room():
    global solver
    global ready
    global room
    solver = generator.solve(a.get())
    ready = False
    room = None


new_room()


def draw_room(room):
    for y in range(room.ymin, room.ymax + 1):
        for x in range(room.xmin, room.xmax + 1):
            t = room.get(x, y)
            if t in tiles:
                r: pygame.Rect = tiles[t].get_rect(topleft=(x * spritescale, y * spritescale))
                screen.blit(tiles[t], r)


loading_rect: pygame.Rect = tiles["o"].get_rect(topleft=(13,18))
loading_speed = [2, 3]


def draw_loading():
    global loading_rect
    loading_rect = loading_rect.move(loading_speed)
    if loading_rect.left < 0 or loading_rect.right > width:
        loading_speed[0] = -loading_speed[0]
    if loading_rect.top < 0 or loading_rect.bottom > height:
        loading_speed[1] = -loading_speed[1]
    screen.blit(tiles["o"], loading_rect)


while 1:
    if not ready:
        secs_spent = clock.get_time() / 1000
        secs_remaining = (1 / loading_framerate) - secs_spent
        secs_remaining = secs_remaining - secs_remaining / 100
        while secs_remaining < 0:
            secs_remaining = secs_remaining + (1 / loading_framerate)
        ready = solver.wait(secs_remaining)

    clock.tick(framerate)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_r:
                new_room()

    if ready:
        room = solver.get_room()

    screen.fill(black)
    if room:
        draw_room(room)
    else:
        draw_loading()
    pygame.display.flip()
