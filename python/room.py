import random
from itertools import product

import clingo

N = clingo.Number
F = clingo.Function


def print_config(c):
    d = "__desc_"
    for key in c.keys:
        print(key + ': ' + c.__getattribute__(d + key))
        g = c.__getattribute__(key)
        if type(g) == clingo.Configuration:
            print(key + ': {')
            print_config(g)
            print('}')
        else:
            print(key + ': ' + str(g))


class Matrix:
    m = {}
    xmin = None
    xmax = None
    ymin = None
    ymax = None
    empty = True

    def put(self, x, y, v):
        if self.empty:
            self.xmin = x
            self.xmax = x
            self.ymin = y
            self.ymax = y
            self.empty = False
        if x < self.xmin:
            self.xmin = x
        if x > self.xmax:
            self.xmax = x
        if y < self.ymin:
            self.ymin = y
        if x > self.ymax:
            self.ymax = y
        self.m[str(x) + ',' + str(y)] = v

    def print(self):
        if self.empty:
            print("'Empty matrix'")
            return
        for y in range(self.ymin, self.ymax + 1):
            for x in range(self.xmin, self.xmax + 1):
                v = self.get(x, y)
                if v:
                    if v == 'o':
                        v = '.'
                    if v == 'x':
                        v = '#'
                    print(v, end=' ')
                else:
                    print(' ', end=' ')
            print('')

    def get(self, x, y):
        k = str(x) + ',' + str(y)
        if k in self.m:
            return self.m[k]
        else:
            return None


class RoomSolver:
    def __init__(self):
        self.solver = None
        self.matrix = Matrix()

    def _set_solver(self, solver):
        self.solver: clingo.SolveHandle = solver

    def _on_model(self, model):
        def tile_char(x, y, c):
            self.matrix.put(x.number, y.number, str(c)[-1])

        print("Model")
        for symbol in model.symbols(shown=True):
            print(symbol)
            if symbol.name == "tile_char":
                tile_char(symbol.arguments[0], symbol.arguments[1], symbol.arguments[2])

    def _on_finish(self, result):
        print("Finished")
        print(result)

    def wait(self, time):
        return self.solver.wait(time)

    def get_room(self):
        return self.matrix


class RoomGenerator:
    ctrl = clingo.Control([])

    def __init__(self, filename):
        self.ctrl.configuration.solver.sign_def = 'rnd'
        self.ctrl.configuration.solver.seed = random.randint(0, 10000000)
        self.ctrl.configuration.solver.rand_freq = 0.1
        print("Loading...")
        self.ctrl.load(filename)

    def ground(self):
        print("Grounding...")
        self.ctrl.ground([("base", [])])

    def solve(self, assumptions) -> RoomSolver:
        solver = RoomSolver()

        print("Solving...")
        solver._set_solver(
            self.ctrl.solve(assumptions=assumptions, on_model=solver._on_model, on_finish=solver._on_finish,
                            yield_=False, async=True))
        return solver


class Assumptions:
    DOOR = {"b": F("sBOpen"), "d": F("sDOpen")}
    KEYS = {0: N(0), 1: N(1)}
    DIRS = {"s": F("south"), "n": F("north"), "e": F("east"), "w": F("west")}

    traversables = {}
    open_exits = {}

    def __init__(self):
        pass

    def open(self, truth, dir):
        self.open_exits[dir] = truth

    def traversable(
            self,
            truth=None,
            start_dir=None,
            start_door=None,
            start_keys=None,
            end_dir=None,
            end_door=None,
            end_keys=None):
        if start_dir is None:
            start_dir = self.DIRS.keys()
        if start_door is None:
            start_door = self.DOOR.keys()
        if start_keys is None:
            start_keys = self.KEYS.keys()
        if end_dir is None:
            end_dir = self.DIRS.keys()
        if end_door is None:
            end_door = self.DOOR.keys()
        if end_keys is None:
            end_keys = self.KEYS.keys()
        terms = product(start_dir, start_door, start_keys, end_dir, end_door, end_keys)
        for term in terms:
            self.traversables[tuple(term)] = truth

    def no_switch(self):
        for p in self.traversables.keys():
            if p[1] != p[4]:
                self.traversables[p] = False

    def no_lock(self):
        for p in self.traversables.keys():
            if p[2] > p[5]:
                self.traversables[p] = False

    def no_keys(self):
        for p in self.traversables.keys():
            if p[2] < p[5]:
                self.traversables[p] = False

    def _traversable_term(self, truth, start_dir, start_door, start_keys, end_dir, end_door, end_keys):
        return (clingo.Function("traversable",
                                [self.DIRS[start_dir],
                                 self.DIRS[end_dir],
                                 clingo.Tuple([self.DOOR[start_door], self.KEYS[start_keys]]),
                                 clingo.Tuple([self.DOOR[end_door], self.KEYS[end_keys]])]),
                truth)

    def _exit_term(self, truth, dir):
        if truth:
            return (clingo.Function("open", [self.DIRS[dir]]), True)
        else:
            return (clingo.Function("closed", [self.DIRS[dir]]), True)

    def get(self):

        ass = []
        for p, t in self.traversables.items():
            if t == None:
                continue
            ass.append(self._traversable_term(t, *p))
        for d, t in self.open_exits.items():
            if t == None:
                continue
            ass.append(self._exit_term(t, d))
        return ass


class State:
    def __init__(self, dopen=None, keys=None):
        self.dopen = dopen
        self.keys = keys

    def getDoor(self):
        if self.dopen == True:
            return clingo.Function("sDOpen")
        elif self.dopen == False:
            return clingo.Function("sBOpen")
        else:
            return "_"

    def getKeys(self):
        if self.keys:
            return clingo.Number(self.keys)
        else:
            return "_"

    def get(self):
        return clingo.Tuple([self.getDoor(), self.getKeys()])


if __name__ == "__main__":
    generator = RoomGenerator("../resources/room.ans")
    generator.ground()

    a = Assumptions()
    a.traversable(start_dir="w", end_dir="e", truth=True)
    a.traversable(start_dir="s", end_dir="e", truth=True)
    a.traversable(start_dir="s", end_dir="n", truth=False)
    a.traversable(start_dir="s", end_dir="n", start_keys=[1], end_keys=[0], end_door="b", truth=True)
    # a.open(dir="n",truth=False)

    # a.traversable(start_dir="e",end_dir="w",truth=True)
    # a.traversable(start_dir="s",end_dir="w",truth=False)

    # a.no_switch()
    # a.no_lock()
    a.no_keys()

    solver = generator.solve(a.get())
    t = 0
    while not solver.wait(1):
        t += 1
        print(t)
    room = solver.get_room()
    room.print()
