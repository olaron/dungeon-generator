/*
 * Created by Robin Alonzo on 06/05/2018.
*/

#include "Game.h"

#include <SDL2/SDL.h>

Game::Game() {

}

Game::~Game() {

}


void Game::init(unsigned int width, unsigned int height) {
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Create window
        SDL_CreateWindowAndRenderer(width, height,0,&window,&renderer);
        //Window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == nullptr )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            success = false;
        }
        else{
            SDL_SetRenderDrawColor(renderer,255,255,255,255);
        }
    }

    running = true;

    rect = new SDL_Rect{10,10,32,32};
    //return success;
}

void Game::handleEvents() {
    SDL_Event event;
    while(SDL_PollEvent(&event)){
        if(event.type == SDL_QUIT){
            running = false;
        }
    }
}

void Game::update() {

}

void Game::render() {
    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer,255,255,255,255);
    SDL_RenderFillRect(renderer,rect);
    SDL_RenderPresent(renderer);
}

void Game::clean() {
    delete rect;
    SDL_DestroyRenderer(renderer);
    renderer = nullptr;
    SDL_DestroyWindow(window);
    window = nullptr;
    SDL_Quit();
}

