/*
 * Created by Robin Alonzo on 06/05/2018.
*/

#ifndef PROJECT_GAME_H
#define PROJECT_GAME_H

#include <SDL2/SDL.h>

class Game {
public:
    Game();
    ~Game();

    void init(unsigned int width, unsigned int height);
    void handleEvents();
    void update();
    void render();
    void clean();

    bool isRunning() {
        return running;
    }

private:
    bool running;

    SDL_Window* window;
    SDL_Renderer* renderer;

    SDL_Rect* rect;
};


#endif //PROJECT_GAME_H
