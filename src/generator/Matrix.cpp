/*
 * Created by Robin Alonzo on 01/05/2018.
*/

#include "Matrix.h"

#include <sstream>
using std::ostringstream;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

void Matrix::put(int x, int y, char v) {
    if (v == 'o') {
        v = '.';
    }
    if (v == 'x') {
        v = '#';
    }
    ostringstream stream;
    stream << x << ',' << y;
    string str = stream.str();
    map[str] = v;
    empty = false;
    if (x < xmin){
        xmin = x;
    }
    if (x > xmax){
        xmax = x;
    }
    if (y < ymin){
        ymin = y;
    }
    if (y > ymax){
        ymax = y;
    }
}

void Matrix::print() {
    if (empty){
        cout << "'Empty matrix'" << endl;
    } else {
        for (int y = ymin; y <= ymax; ++y) {
            for (int x = xmin; x <= xmax; ++x) {
                ostringstream stream;
                stream << x << ',' << y;
                std::string str = stream.str();
                char c = map.at(str);
                cout << c << ' ';
            }
            cout << endl;
        }
    }
}