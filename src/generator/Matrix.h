/*
 * Created by Robin Alonzo on 01/05/2018.
*/

#ifndef PROJECT_MATRIX_H
#define PROJECT_MATRIX_H

#include <climits>

#include <unordered_map>
using std::unordered_map;

#include <string>
using std::string;

class Matrix {
private:
    unordered_map<string,char> map;
    int xmin = INT_MAX;
    int xmax = INT_MIN;
    int ymin = INT_MAX;
    int ymax = INT_MIN;
    bool empty = true;

public:
    void put(int x, int y, char v);

    void print();
};

#endif //PROJECT_MATRIX_H
