/*
 * Created by Robin Alonzo on 01/05/2018.
*/

#include "RoomGenerator.h"

#include <clingo.hh>
using namespace Clingo;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <vector>
using std::vector;

#include <tuple>
using std::pair;

#include <sstream>
using std::ostringstream;


Control* RoomGenerator::control;

RoomGenerator::RoomSolveHandler::RoomSolveHandler(Matrix &matrix) {
    this->matrix = &matrix;
}

bool RoomGenerator::RoomSolveHandler::on_model(Model const &model) {
    SymbolVector symbols = model.symbols(ShowType::Shown);
    for (Symbol symbol : symbols) {
        string name = symbol.name();
        if (name == "tile_char"){
            int x = symbol.arguments()[0].number();
            int y = symbol.arguments()[1].number();
            char c = symbol.arguments()[2].to_string()[0];
            matrix->put(x,y,c);
        }
    }
    return true;
}

void RoomGenerator::RoomSolveHandler::on_finish(SolveResult result) {
    /* nothing */
}

void RoomGenerator::init() {
    Clingo::Logger logger = [](Clingo::WarningCode, char const *message) {
        std::cerr << message << std::endl;
    };
    control = new Control{{}, logger, 20};
    cout << "Loading..." << endl;
    control->load("resources/room.ans");
    cout << "Loaded" << endl;
    setSeed();
    control->configuration()["solver"]["sign_def"] = "rnd";
    control->configuration()["solver"]["rand_freq"] = "0.1";
    cout << "Grounding..." << endl;
    control->ground({{"base",{}}},[](Location loc, char const * str, SymbolSpan syms, SymbolSpanCallback cb){});
    cout << "Grounded" << endl;
}

void RoomGenerator::free() {
    delete control;
}

void RoomGenerator::setSeed(long long int seed) {
    ostringstream stream;
    stream << seed;
    string str = stream.str();
    control->configuration()["solver"]["seed"] = str.c_str();
}

void RoomGenerator::traversable(const string &from, const string &to, bool truth) {
    SymbolicLiteral symbol{Function("traversable",{Function(from.c_str(),{}),Function(to.c_str(),{})}),truth};
    assumptions.push_back(symbol);
}

void RoomGenerator::closed(const string &direction) {
    SymbolicLiteral symbol{Function("closed",{Function(direction.c_str(),{})}),true};
    assumptions.push_back(symbol);
}

void RoomGenerator::open(const string &direction) {
    SymbolicLiteral symbol{Function("open",{Function(direction.c_str(),{})}),true};
    assumptions.push_back(symbol);
}

Matrix RoomGenerator::generate() {
    Matrix matrix;
    RoomSolveHandler onsolve(matrix);
    control->solve(assumptions,&onsolve,false,false);
    return matrix;
}

Matrix RoomGenerator::generate(long long seed) {
    setSeed(seed);
    return generate();
}