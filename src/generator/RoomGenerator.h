/*
 * Created by Robin Alonzo on 01/05/2018.
*/

#ifndef PROJECT_ROOMGENERATOR_H
#define PROJECT_ROOMGENERATOR_H

#include <ctime>

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <clingo.hh>
using namespace Clingo;

#include "Matrix.h"

class RoomGenerator{
private:

    class RoomSolveHandler: public SolveEventHandler {
    private:
        Matrix *matrix;
    public:

        RoomSolveHandler(Matrix &matrix);

        virtual bool on_model(Model const &model);

        virtual void on_finish(SolveResult result);
    };

    static Control* control;
    vector<SymbolicLiteral> assumptions;
public:

    static void init();

    static void free();

    static void setSeed(long long seed = time(nullptr));

    void traversable(const string& from,const string& to, bool truth);

    void closed(const string& direction);

    void open(const string& direction);

    Matrix generate(long long seed);

    Matrix generate();
};

#endif //PROJECT_ROOMGENERATOR_H
