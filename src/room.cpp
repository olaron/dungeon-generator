#if __EMSCRIPTEN__
#import <emscripten.h>
#endif

#include <clingo.hh>
using namespace Clingo;

#include <fmod.hpp>

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include "generator/RoomGenerator.h"
#include "generator/Matrix.h"
#include "game/Game.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

Game* game = nullptr;

void init()
{

    game = new Game();
    game->init(SCREEN_WIDTH,SCREEN_HEIGHT);
}

void loop(){
    if(game->isRunning()){
        game->handleEvents();
        game->update();
        game->render();
    }else{
        game->clean();
    }
}

/* main */

int main(int argc, char ** argv) {
    try {
        //The window we'll be rendering to
        init();
#if __EMSCRIPTEN__
        // void emscripten_set_main_loop(em_callback_func func, int fps, int simulate_infinite_loop);
        emscripten_set_main_loop(loop, 0, 1);
#else
        while (game->isRunning()) {
            loop();
            // Delay to keep frame rate constant (using SDL)
            //SDL_Delay(time_to_next_frame());
            SDL_Delay(16);
        }
#endif
    }
    catch (std::exception const &e) {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}